import { registerPlugin } from '@capacitor/core';

import type { DayClockUpgradePlugin } from './definitions';

const DayClockUpgrade = registerPlugin<DayClockUpgradePlugin>(
  'DayClockUpgrade',
  {
    web: () => import('./web').then(m => new m.DayClockUpgradeWeb()),
  },
);

export * from './definitions';
export { DayClockUpgrade };
