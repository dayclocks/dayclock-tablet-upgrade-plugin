# dayclock-tablet-upgrade

upgrade of DayClock app

## Install

```bash
npm install dayclock-tablet-upgrade
npx cap sync
```

## API

<docgen-index>

* [`upgrade()`](#upgrade)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### upgrade()

```typescript
upgrade() => any
```

**Returns:** <code>any</code>

--------------------

</docgen-api>
