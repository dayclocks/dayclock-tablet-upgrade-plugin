package nl.dayclocks.tablet.upgrade;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "DayClockUpgrade")
public class DayClockUpgradePlugin extends Plugin {

    private DayClockUpgrade implementation = new DayClockUpgrade();

    @PluginMethod
    public void upgrade() {
        System.out.println("upgrade")
        SObject ret = new JSObject();
        ret.put("added", true);
        JSObject info = new JSObject();
        info.put("id", "unique-id-1234");
        ret.put("info", info);
        call.resolve(ret);
    }
}
